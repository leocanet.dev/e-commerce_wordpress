<?php 


/**
 * Plugin Name: Custom Order Statuses for WooCommerce
 * Author: Nuggethon
 * Description: Allows to create Custom Order Statuses for WooCommerce and send Custom Emails for them.
 * Version: 1.3.0
 * Text Domain: custom-order-statuses-for-woocommerce
 * Domain Path: /languages
 * Requires at least: 5.3
 * Tested up to: 5.9
 * Requires PHP: 5.6.40
 * Requires WC at least: 3.8.1
 * WC tested up to: 6.3.1
 * 
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */


 


 
add_action( 'init', 'wpdocs_load_textdomain' );
 
function wpdocs_load_textdomain() {
    load_plugin_textdomain( 'custom-order-statuses-for-woocommerce', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' ); 
}

include('assets.php');
include('functions.php');

require('email-manager.php');
require('order-status-generator.php');

require('ajax/bulk-change-status.php');
require('ajax/deactivation-form.php');
require('ajax/update-woocos-item.php');
require('ajax/expand-woocos-item.php');
require('ajax/remove-woocos-item.php');


require_once('settings-page.php');

register_activation_hook(__FILE__ , 'woocos_remove_woocos_prefix' );

function woocos_remove_woocos_prefix()
{
    foreach(wp_load_alloptions() as $option => $key) {
        if (strpos($option, 'woocommerce_woocos-') !== false) {
            $option_data = get_option($option, array());
            $option_name = str_replace('woocos-', '', $option);
           
            update_option($option_name, $option_data);
            $slug = str_replace('woocommerce_woocos-', '', $option);
            $slug = str_replace('_settings', '', $slug);
            delete_option($option);
        }
    }

    $woocos_options =  json_decode(get_option('woocos_custom_order_statuses'), true);
    if (!empty($woocos_options)) {
        foreach ( $woocos_options as $key => $option ) {
            $slug = 'woocos-' . $option['slug'];
            $args = array(
                'status'    => $slug,
                'limit'     => -1,
            );
            $orders = wc_get_orders($args);
            if ( !empty( $orders ) ) {
                foreach ( $orders as $order ) {
                    if ( $order->get_status() == $slug) {
                        $temp_slug = 'wc-' . $option['slug'];
                        $args = array(
                            'ID'            => $order->get_id(),
                            'post_status'   => $temp_slug
                        );
                        wp_update_post($args);
                    }
                }
            }
        }
    }
}