1. Renommer le dossier "wordpress" en "wordpress_oxygen" puis procéder à l'installation de WP.

2. Remplacer le dossier WP-content par celui présent dans le dépôt GIT.

3. Activer les extensions.

4. Blank installations Oxygen > Oxygen > Export & Import > Supprimer le code dans Export global settings > Coller le code présent dans le fichier globalsettings.txt dans import + Cocher la case import Global Colors > Import Global settings.

5. Outil > Importer > Wordpress Installer maintenant > Lancer l’outil >  Importer le fichier XML. > Téléverser et importer les fichiers joints.

6. Import ACF : ACF > Outil > Importer le fichier "acf-export" présent dans le dossier "Import" Si il ne s’affiche pas dans Page > modifier -> se rendre dans les Custom fields pour les mettre à jour manuellement pour qu'ils puissent s'activer sur votre page accueil.

7. Supprimer les pages en double ou inutile : Les pages Woocommerce sans la balise “- TitrePage”  -> Doublon WooCommerce. Ainsi que la page d’exemple, politique de confidentialité. Ainsi que dans “Articles” supprimer le premier post “Bonjour tout le monde”.

8. Oxygen > Security > "sign all shortcodes" pour récupérer le style des templates manquants > Cocher “I have made a complete backup of my site”, “post”, “page” et “ct_template”.
https://oxybuilderfrancais.com/documentation/autres/importer-exporter/

9. Réglages > Lecture Page d'accueil affiche > une page statique > page d'accueil : sélectionner "Accueil".

10. Apparence menu > Menu, refaire le menu “navigation” et “footer” avec les pages boutique et panier valide ainsi que FiboSearchBar pour “navigation”.

11. Si la barre de recherche ne s'affiche pas dans le header > Oxygen > Templates > edit main template with oxygen > Cliquer sur la navbar dans le header > Dans l’interface à gauche de votre écran “Menu” > Sélectionnez “navigation” > Save.

12. Pour corriger les “a” présents dans les page d’accueil et d’articles > Pages > Accueil/Articles > Edit with oxygen > Cliquer sur les différents éléments (button, a) pour changer le href avec “set” puis choisir la bonne page.

13. Activer stripe en mode test avec ses clés de comptes. WooCommerce > Réglages > Paiement > Activer Stripe payement card.

14. Créer un compte pour le rédacteur des articles et lui définir le rôle de "auteur".
